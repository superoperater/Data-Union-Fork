**Data Union Fork → Tools for data strike**_
 is a participatory artwork created by Larisa Blazic in collaboration with Waag society and Dyne.org, supported by STARTS Residency programme and hosted by DECODE project. 
 
The work produced during a year and a half long residency consists of an interactive installation and workshops that together create a narrative supporting DECODE project’s argument for algorithmic sovereignty and data commons on one hand and more broadly it advocates for a fair and sustainable digital transition, putting data and innovation at the service of the people. 

The design and implementation for the artwork was structured in three development stages, each focused on the completion of specific components of the work. The second of these stages involved public participation facilitated by Waag Society. Dyne.org (DECODE project’s technical lead) Free/Libre/Open Software (FLOSS) culture and practice enabled remote collaboration through publicly released materials and code. 


Supported by [Vertigo STARTS](https://vertigo.starts.eu/calls/2017-2/residencies/data-union-fork-tools-for-data-strike/detail/). 

Facilitated by [Waag Society](https://waag.org/nl/larisa-blazic)

[DECODE project](https://decodeproject.eu/)

[Dyne.org ](https://www.dyne.org/)

